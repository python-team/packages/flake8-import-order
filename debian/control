Source: flake8-import-order
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jose Luis Rivero <jrivero@osrfoundation.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pycodestyle,
               python3-all,
               python3-flake8,
               python3-setuptools,
               python3-pylama,
               python3-pytest
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/flake8-import-order
Vcs-Git: https://salsa.debian.org/python-team/packages/flake8-import-order.git
Rules-Requires-Root: no
Homepage: https://github.com/PyCQA/flake8-import-order
Testsuite: autopkgtest-pkg-pybuild

Package: python3-flake8-import-order
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Enhances: flake8, python3-pylama
Description: flake8 extension to lint for the import order in Python files
 A flake8 and Pylama plugin that checks the ordering of Python imports. It
 does not check anything else about the imports. Merely that they are
 grouped and ordered correctly.
 .
 In general stdlib comes first, then 3rd party, then local packages, and
 that each group is individually alphabetized, however this depends on
 the style used. Flake8-Import-Order supports a number of styles and is
 extensible allowing for custom styles.
